/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TicTacToe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Oriol Mainou
 */
public class Juego {

    BufferedReader br;

    public char[][] tablero;
    public boolean turnoJugador = true, partidaFinalizada = false;
    public char fichaJugador1 = 'X', fichaJugador2 = 'O';
    public int numeroFilas = 3, numeroColumnas = 3;

    public Juego() {
        BufferedReader buffered_reader = new BufferedReader(new InputStreamReader(System.in));
        br = buffered_reader;
    }

    public char getFichaJugador() {
        return (turnoJugador == true) ? fichaJugador1 : fichaJugador2;
    }

    public void nuevaPartida() {
        turnoJugador = true;
        tablero = new char[numeroFilas][numeroColumnas];

        for (int i = 0; i < numeroFilas; i++) {
            for (int j = 0; j < numeroColumnas; j++) {
                tablero[i][j] = ' ';
            }
        }
    }

    public void dibujarTablero() {
        for (int i = 0; i < numeroFilas; i++) {
            for (int j = 0; j < numeroColumnas; j++) {
                System.out.print(tablero[i][j]);
                if (j == 2) {
                    System.out.println("");
                }
            }
        }
    }

    public void pedirJugada() throws IOException {
        int posicionFila, posicionColumna;
        if (turnoJugador == true) {
            System.out.println("Jugador 1 (" + fichaJugador1 + ")");
        } else {
            System.out.println("Jugador 2 (" + fichaJugador2 + ")");
        }

        System.out.print("Fila para la ficha (1,2,3): ");
        String cadena = br.readLine();
        posicionFila = Integer.parseInt(cadena);
        posicionFila--;
        System.out.print("Columna para la ficha (1,2,3): ");
        String cadena2 = br.readLine();
        posicionColumna = Integer.parseInt(cadena2);
        posicionColumna--;

        boolean jugadaConExito = escribirJugada(posicionFila, posicionColumna);

        while (jugadaConExito == false) {
            System.out.print("Fila para la ficha (1,2,3): ");
            cadena = br.readLine();
            posicionFila = Integer.parseInt(cadena);
            posicionFila--;
            System.out.print("Columna para la ficha (1,2,3): ");
            cadena2 = br.readLine();
            posicionColumna = Integer.parseInt(cadena2);
            posicionColumna--;
            jugadaConExito = escribirJugada(posicionFila, posicionColumna);
        }
    }

    public void cambioTurno() {
        turnoJugador = !turnoJugador;
    }

    public boolean escribirJugada(int posicionFila, int posicionColumna) {
        boolean exito = true;
        if (posicionFila >= 0 && posicionColumna >= 0 && posicionFila <= 2 && posicionColumna <= 2) {
            if (tablero[posicionFila][posicionColumna] == ' ') {
                tablero[posicionFila][posicionColumna] = (turnoJugador == true) ? fichaJugador1 : fichaJugador2;
            } else {
                exito = false;
                System.out.println("Error. Esta posicion ya está ocupada");
            }
        } else {
            exito = false;
            System.out.println("Error. Posición fuera de rango");
        }
        return exito;
    }

    public boolean esVictoria() {
        boolean victoria = false;
        char fichaJugador = getFichaJugador();
        int contadorFicha = 0;

        for (int i = 0; i < numeroFilas; i++) {
            for (int j = 0; j < numeroColumnas; j++) {
                if (tablero[i][j] == fichaJugador) {
                    contadorFicha++;
                }
            }
            if (contadorFicha == 3) {
                victoria = true;
            }
            contadorFicha = 0;
        }

        for (int i = 0; i < numeroColumnas; i++) {
            for (int j = 0; j < numeroFilas; j++) {
                if (tablero[i][j] == fichaJugador) {
                    contadorFicha++;
                }
            }
            if (contadorFicha == 3) {
                victoria = true;
            }
            contadorFicha = 0;
        }

        if (tablero[0][0] == fichaJugador && tablero[1][1] == fichaJugador && tablero[2][2] == fichaJugador) {
            victoria = true;
        }

        if (tablero[0][2] == fichaJugador && tablero[1][1] == fichaJugador && tablero[2][0] == fichaJugador) {
            victoria = true;
        }
        return victoria;
    }

    public boolean tableroLleno() {
        boolean lleno = true;

        for (int f = 0; f < numeroFilas; f++) {
            for (int c = 0; c < numeroColumnas; c++) {
                if (tablero[f][c] == ' ') {
                    lleno = false;
                }
            }
        }
        return lleno;
    }

    public int getJugadorActual() {
        int jugador;

        if (turnoJugador) {
            jugador = 1;
        } else {
            jugador = 2;
        }
        return jugador;
    }
}
