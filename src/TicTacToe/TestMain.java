/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TicTacToe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Oriol Mainou
 */
public class TestMain {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        boolean salir = false;

        Juego j1 = new Juego();

        j1.nuevaPartida();

        while (salir != true) {
            j1.pedirJugada();
            j1.dibujarTablero();

            if (j1.esVictoria() == true) {
                System.out.println("Victoria del jugador " + j1.getJugadorActual());
                salir = true;
            } else if (j1.tableroLleno() == true) {
                System.out.println("Tablero lleno. Empate");
                salir = true;

            } else {
                j1.cambioTurno();
            }
        }

    }
}
