
import TicTacToe.Juego;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Oriol Mainou
 */
public class CambioTurnoTest extends Juego {

    @Test
    public void CambioTurno() {
        super.nuevaPartida();
        super.cambioTurno();

        Assert.assertFalse(super.turnoJugador);
    }

    @Test
    public void CambioSegundoTurno() {
        super.nuevaPartida();
        super.cambioTurno();
        super.cambioTurno();

        Assert.assertTrue(super.turnoJugador);
    }
}
