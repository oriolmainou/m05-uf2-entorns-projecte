import TicTacToe.Juego;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Oriol Mainou
 */
public class EscribirJugadaTest extends Juego {

    boolean resultado;

    @Test
    public void ErrorFueraTablero() {
        super.nuevaPartida();
        resultado = super.escribirJugada(super.numeroColumnas + 1, super.numeroFilas + 1);

        Assert.assertFalse(resultado);
    }

    @Test
    public void ErrorEscribirNumeroNegativo() {
        super.nuevaPartida();
        resultado = super.escribirJugada(-1, -8);

        Assert.assertFalse(resultado);
    }

    @Test
    public void PosicionEstaOcupada() {
        super.nuevaPartida();
        super.tablero[1][1] = super.fichaJugador1;
        resultado = super.escribirJugada(1, 1);

        Assert.assertFalse(resultado);
    }

    @Test
    public void Jugada_valida() {
        super.nuevaPartida();
        resultado = super.escribirJugada(1, 1);

        Assert.assertTrue(resultado);
    }
}
