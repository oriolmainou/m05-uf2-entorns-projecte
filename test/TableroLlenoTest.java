
import TicTacToe.Juego;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Oriol Mainou
 */
public class TableroLlenoTest extends Juego {

    @Test
    public void CasillasLlenas() {
        super.nuevaPartida();
        boolean estaLleno = true;

        for (int i = 0; i < numeroFilas; i++) {
            for (int k = 0; k < numeroColumnas; k++) {
                tablero[i][k] = super.fichaJugador1;
            }
        }

        estaLleno = super.tableroLleno();
        Assert.assertTrue(estaLleno);
    }

    @Test
    public void TableroCasiLleno() {
        super.nuevaPartida();
        boolean estaLleno = true;

        super.tablero[0][0] = fichaJugador1;
        super.tablero[2][2] = fichaJugador1;

        estaLleno = super.tableroLleno();
        Assert.assertFalse(estaLleno);
    }
}