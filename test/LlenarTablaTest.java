import TicTacToe.Juego;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Oriol Mainou
 */
public class LlenarTablaTest extends Juego {

    public LlenarTablaTest() {
        super();
    }

    @Test
    public void revisarTabla() {
        super.nuevaPartida();
        boolean error = true;

        for (int i = 0; i < super.numeroFilas; i++) {
            if (error == false) {
                break;
            }
            for (int k = 0; k < super.numeroColumnas; k++) {
                if (super.tablero[i][k] != ' ') {
                    error = true;
                } else {
                    error = false;
                    break;
                }
            }
        }
        Assert.assertFalse(error);
    }
}
