
import TicTacToe.Juego;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Oriol Mainou
 */
public class VictoriaTest extends Juego {

    boolean esVictoria;

    @Test
    public void VictoriaPorDiagonalPrimerTest() {
        esVictoria = false;
        super.nuevaPartida();
        
        super.tablero[0][0] = fichaJugador1;
        super.tablero[1][1] = fichaJugador1;
        super.tablero[2][2] = fichaJugador1;

        esVictoria = super.esVictoria();

        Assert.assertTrue(esVictoria);
    }

    @Test
    public void VictoriaPorDiagonalSegundoTest() {
        esVictoria = false;
        super.nuevaPartida();
        super.tablero[0][2] = fichaJugador1;
        super.tablero[1][1] = fichaJugador1;
        super.tablero[2][0] = fichaJugador1;

        esVictoria = super.esVictoria();

        Assert.assertTrue(esVictoria);
    }

    @Test
    public void VictoriaPorDiagonalDelJugador2() {
        esVictoria = false;
        super.nuevaPartida();
        super.turnoJugador = false;
        super.tablero[0][2] = fichaJugador2;
        super.tablero[1][1] = fichaJugador2;
        super.tablero[2][0] = fichaJugador2;

        esVictoria = super.esVictoria();

        Assert.assertTrue(esVictoria);
    }

    @Test
    public void NoVictoria() {
        esVictoria = false;
        super.nuevaPartida();
        super.turnoJugador = false;
        super.tablero[0][2] = fichaJugador2;
        super.tablero[1][1] = fichaJugador2;
        super.tablero[2][0] = fichaJugador1;

        esVictoria = super.esVictoria();

        Assert.assertFalse(esVictoria);
    }
}
